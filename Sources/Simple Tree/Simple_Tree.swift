public class StNode<T: Equatable> {
    private var name: T?
    private weak var parent: StNode?
    private var children: [StNode] = []
    private var isRoot: Bool
    private var isLeaf: Bool

    public init(name: T?) {
        self.name = name
        isRoot = true
        isLeaf = true
    }

    deinit {
        for node in children {
            node.warnParentUnallocated()
        }
    }

    private func warnParentUnallocated() {
        parent = nil
    }

    public func getName() -> T? {
        name
    }

    public func getRootStatus() -> Bool {
        isRoot
    }

    public func getLeaveStatus() -> Bool {
        isLeaf
    }

    public func getParent() -> StNode? {
        parent
    }

    public func getChildren() -> [StNode] {
        children
    }

    public func addChild(child: StNode) {
        if(children.filter {$0 === child}.count == 0) {
            children.append(child)
            child.parent = self
            child.isRoot = false
            isLeaf = false
        }
    }

    public func removeChild(child: StNode) {
        children = children.filter { node in
            return node !== child
        }
        child.parent = nil
    }

    public func removeChildbyName(name: T?) {
        let candidates = children.filter { node in
            return node.name == name
        }

        for node in candidates {
            node.parent = nil
        }

        children = children.filter { node in
            return node.name != name
        }
    }

    public func findByNameinChildren(name: T?) -> [StNode] {
        let result = children.filter { node in
            return node.name == name
        }

        return result
    }

    public func findByNameinTree(name: T?) -> [StNode] {
        var result: [StNode] = []
        var stack : [StNode] = []
        var working : StNode = self

        repeat{
            if (working.getName() == name){
                result.append(working)
            }
            stack += working.getChildren()
            if(stack.isEmpty){
                break
            }else{
                working = stack.popLast()!
            }
            

        }while(true)

        return result
    }

    public func leaves() -> [StNode] {
        var result: [StNode] = []
        var stack : [StNode] = []
        var working : StNode = self

        repeat{
            if (working.isLeaf){
                result.append(working)
            }else{
                stack += working.getChildren()
            }
            
            if(stack.isEmpty){
                break
            }else{
                working = stack.popLast()!
            }
            

        }while(true)
        
        return result
    }

}
