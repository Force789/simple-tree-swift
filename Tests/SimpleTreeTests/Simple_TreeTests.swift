import XCTest
@testable import Simple_Tree

final class Simple_TreeTests: XCTestCase {
    
    func testcorrectCreation(){
        let node = StNode(name: "A")
        XCTAssertEqual(node.getName(), "A")
        XCTAssertNil(node.getParent())
        XCTAssertEqual(node.getChildren().count, 0)
    }
    
    func testTreeFiliation(){
        let node = StNode(name: "A")
        node.addChild(child: StNode(name: "B"))
        node.addChild(child: StNode(name: "D"))
        node.getChildren()[0].addChild(child: StNode(name: "C"))
        XCTAssertEqual(node.getName(), "A")
        XCTAssertEqual(node.getChildren().count, 2)
        XCTAssertEqual(node.getChildren()[0].getName(), "B")
        XCTAssertEqual(node.getChildren()[1].getName(), "D")
        XCTAssertEqual(node.getChildren()[0].getChildren()[0].getName(), "C")
    }

    static var allTests = [
        ("correctCreation", testcorrectCreation),
        ("testTreeFiliation", testTreeFiliation),
    ]
}
