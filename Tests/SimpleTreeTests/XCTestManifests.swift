import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(Simple_TreeTests.allTests),
    ]
}
#endif
